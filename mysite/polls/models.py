import datetime

from django.contrib import admin
from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField(
        max_length=200, verbose_name="Text of the question"
    )
    pub_date = models.DateTimeField(verbose_name="date published")

    def __str__(self):
        return self.question_text

    @admin.display(
        boolean=True,
        ordering="pub_date",
        description="Published recently?",
    )
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now


class Choice(models.Model):
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, verbose_name="Question"
    )
    choice_text = models.CharField(max_length=200, verbose_name="Text of the choice")
    votes = models.IntegerField(default=0, verbose_name="Number of votes")

    def __str__(self):
        return self.choice_text
