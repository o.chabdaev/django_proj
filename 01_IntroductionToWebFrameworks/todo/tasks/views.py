from django.http import HttpResponse

from django.views import View

from random import shuffle


class ToDoView(View):

    def get(self, request, *args, **kwargs):
        tasks = [
            'Установить python',
            'Установить django',
            'Запустить сервер',
            'Порадоваться результату',
            'Придумать задания',
            'Разобраться с HttpRespons',
            'Написать views',
            'Проверить результат',
            'Устранить ошибки',
            'Вдохновиться',
        ]
        tasks_temp = tasks[:]
        result = '<ul>'
        for _ in range(5):
            shuffle(tasks_temp)
            result += '<li>' + tasks_temp.pop() + '</li>'
        result += '</ul>'
        return HttpResponse(result)
